<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script type="text/javascript">
    var count = 1, an = 1, po_edit = true, product_variant = 0, DT = <?= $Settings->default_tax_rate ?>, DC = '<?=$default_currency->code?>', shipping = 0,
        product_tax = 0, invoice_tax = 0, total_discount = 0, total = 0,
        tax_rates = <?php echo json_encode($tax_rates); ?>, poitems = {},
        audio_success = new Audio('<?= $assets ?>sounds/sound2.mp3'),
        audio_error = new Audio('<?= $assets ?>sounds/sound3.mp3');
    $(window).bind("load", function() {
        <?= ($inv->status == 'received' || $inv->status == 'partial') ? '$(".rec_con").show();' : '$(".rec_con").hide();'; ?>
    });
    $(document).ready(function () {
        <?= ($inv->status == 'received' || $inv->status == 'partial') ? '$(".rec_con").show();' : '$(".rec_con").hide();'; ?>
        $('#postatus').change(function(){
            var st = $(this).val();
            if (st == 'received' || st == 'partial') {
                $(".rec_con").show();
            } else {
                $(".rec_con").hide();
            }
        });
        <?php if ($inv) { ?>
        localStorage.setItem('podate', '<?= date($dateFormats['php_ldate'], strtotime($inv->date))?>');
        localStorage.setItem('posupplier', '<?=$inv->supplier_id?>');
        localStorage.setItem('poref', '<?=$inv->reference_no?>');
        localStorage.setItem('powarehouse', '<?=$inv->warehouse_id?>');
        localStorage.setItem('postatus', '<?=$inv->status?>');
        localStorage.setItem('ponote', '<?= str_replace(array("\r", "\n"), "", $this->sma->decode_html($inv->note)); ?>');
        localStorage.setItem('podiscount', '<?=$inv->order_discount_id?>');
        localStorage.setItem('potax2', '<?=$inv->order_tax_id?>');
        localStorage.setItem('poshipping', '<?=$inv->shipping?>');
        localStorage.setItem('popayment_term', '<?=$inv->payment_term?>');
        if (parseFloat(localStorage.getItem('potax2')) >= 1 || localStorage.getItem('podiscount').length >= 1 || parseFloat(localStorage.getItem('poshipping')) >= 1) {
            localStorage.setItem('poextras', '1');
        }
        localStorage.setItem('poitems', JSON.stringify(<?=$inv_items;?>));
        <?php } ?>

        <?php if ($Owner || $Admin) { ?>
        $(document).on('change', '#podate', function (e) {
            localStorage.setItem('podate', $(this).val());
        });
        if (podate = localStorage.getItem('podate')) {
            $('#podate').val(podate);
        }
        <?php } ?>
        ItemnTotals();
        $("#add_item").autocomplete({
            source: '<?= admin_url('purchases/suggestions'); ?>',
            minLength: 1,
            autoFocus: false,
            delay: 250,
            response: function (event, ui) {
                if ($(this).val().length >= 16 && ui.content[0].id == 0) {
                    bootbox.alert('<?= lang('no_match_found') ?>', function () {
                        $('#add_item').focus();
                    });
                    $(this).removeClass('ui-autocomplete-loading');
                    $(this).val('');
                }
                else if (ui.content.length == 1 && ui.content[0].id != 0) {
                    ui.item = ui.content[0];
                    $(this).data('ui-autocomplete')._trigger('select', 'autocompleteselect', ui);
                    $(this).autocomplete('close');
                    $(this).removeClass('ui-autocomplete-loading');
                }
                else if (ui.content.length == 1 && ui.content[0].id == 0) {
                    bootbox.alert('<?= lang('no_match_found') ?>', function () {
                        $('#add_item').focus();
                    });
                    $(this).removeClass('ui-autocomplete-loading');
                    $(this).val('');
                }
            },
            select: function (event, ui) {
                event.preventDefault();
                if (ui.item.id !== 0) {
                    var row = add_purchase_item(ui.item);
                    if (row)
                        $(this).val('');
                } else {
                    bootbox.alert('<?= lang('no_match_found') ?>');
                }
            }
        });

        $(document).on('click', '#addItemManually', function (e) {
            if (!$('#mcode').val()) {
                $('#mError').text('<?=lang('product_code_is_required')?>');
                $('#mError-con').show();
                return false;
            }
            if (!$('#mname').val()) {
                $('#mError').text('<?=lang('product_name_is_required')?>');
                $('#mError-con').show();
                return false;
            }
            if (!$('#mcategory').val()) {
                $('#mError').text('<?=lang('product_category_is_required')?>');
                $('#mError-con').show();
                return false;
            }
            if (!$('#munit').val()) {
                $('#mError').text('<?=lang('product_unit_is_required')?>');
                $('#mError-con').show();
                return false;
            }
            if (!$('#mcost').val()) {
                $('#mError').text('<?=lang('product_cost_is_required')?>');
                $('#mError-con').show();
                return false;
            }
            if (!$('#mprice').val()) {
                $('#mError').text('<?=lang('product_price_is_required')?>');
                $('#mError-con').show();
                return false;
            }

            var msg, row = null, product = {
                type: 'standard',
                code: $('#mcode').val(),
                name: $('#mname').val(),
                tax_rate: $('#mtax').val(),
                tax_method: $('#mtax_method').val(),
                category_id: $('#mcategory').val(),
                unit: $('#munit').val(),
                cost: $('#mcost').val(),
                price: $('#mprice').val()
            };

            $.ajax({
                type: "get", async: false,
                url: site.base_url + "products/addByAjax",
                data: {token: "<?= $csrf; ?>", product: product},
                dataType: "json",
                success: function (data) {
                    if (data.msg == 'success') {
                        row = add_purchase_item(data.result);
                    } else {
                        msg = data.msg;
                    }
                }
            });
            if (row) {
                $('#mModal').modal('hide');
            } else {
                $('#mError').text(msg);
                $('#mError-con').show();
            }
            return false;

        });
        $(window).bind('beforeunload', function (e) {
            $.get('<?=admin_url('welcome/set_data/remove_pols/1');?>');
            if (count > 1) {
                var message = "You will loss data!";
                return message;
            }
        });
        $('#reset').click(function (e) {
            $(window).unbind('beforeunload');
        });
        $('#edit_pruchase').click(function () {
            $(window).unbind('beforeunload');
            $('form.edit-po-form').submit();
        });

    });


</script>

<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-edit"></i><?= lang('edit_purchase'); ?></h2>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">

                <p class="introtext"><?php echo lang('enter_info'); ?></p>
                <?php
                $attrib = array('data-toggle' => 'validator', 'role' => 'form', 'class' => 'edit-po-form');
                echo admin_form_open_multipart("purchases/edit/" . $inv->id, $attrib)
                ?>

                <div class="row">
                    <div class="col-lg-12">

                        <?php if ($Owner || $Admin) { ?>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("date", "podate"); ?>
                                    <?php echo form_input('date', (isset($_POST['date']) ? $_POST['date'] : $this->sma->hrld($purchase->date)), 'class="form-control input-tip datetime" id="podate" required="required"'); ?>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="col-md-4 hidden">
                            <div class="form-group">
                                <?= lang("reference_no", "poref"); ?>
                                <?php echo form_input('reference_no', (isset($_POST['reference_no']) ? $_POST['reference_no'] : $purchase->reference_no), 'class="form-control input-tip" id="poref" required="required"'); ?>
                            </div>
                        </div>
					<?php if ($Owner || $Admin || !$this->session->userdata('biller_id')) { ?>
							<div class="col-md-4 <?php if(!$Owner && !$Admin): ?> hidden<?php endif; ?>">
								<div class="form-group">
									<?= lang("biller", "slbiller"); ?>
									<?php echo $purchase_biller; ?>
									<?php
									$bl[""] = "";
									foreach ($billers as $biller) {
										$bl[$biller->id] = $biller->first_name." ".$biller->last_name."(".$biller->email.")";
									}
									echo form_dropdown('pobiller', $bl, (isset($purchase_biller) ? $purchase_biller->id : ''), 'id="pobiller" data-placeholder="' . lang("select") . ' ' . lang("biller") . '" required="required" class="form-control input-tip select" style="width:100%;"');
									?>
								</div>
							</div>
						<?php } else {
							$biller_input = array(
								'type' => 'hidden',
								'name' => 'biller',
								'id' => 'slbiller',
								'value' => $this->session->userdata('biller_id'),
							);

							echo form_input($biller_input);
						} ?>

						<div class="clearfix"></div>	
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("warehouse", "powarehouse"); ?>
                                <?php
                                $wh[''] = '';
                                foreach ($warehouses as $warehouse) {
                                    $wh[$warehouse->id] = $warehouse->name;
                                }
                                echo form_dropdown('warehouse', $wh, (isset($_POST['warehouse']) ? $_POST['warehouse'] : $purchase->warehouse_id), 'id="powarehouse" class="form-control input-tip select" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("warehouse") . '" required="required" style="width:100%;" ');
                                ?>
                            </div>
                        </div>
                    <?php /*    <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("status", "postatus"); ?>
                                <?php
                                $post = array('received' => lang('received'), 'partial' => lang('partial'), 'pending' => lang('pending'), 'ordered' => lang('ordered'));
                                echo form_dropdown('status', $post, (isset($_POST['status']) ? $_POST['status'] : $purchase->status), 'id="postatus" class="form-control input-tip select" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("status") . '" required="required" style="width:100%;" ');
                                ?>
                            </div>
                        </div>*/ ?>

                        <?php /*<div class="col-md-4">
                            <div class="form-group">
                                <?= lang("document", "document") ?>
                                <input id="document" type="file" data-browse-label="<?= lang('browse'); ?>" name="document" data-show-upload="false"
                                       data-show-preview="false" class="form-control file">
                            </div>
                        </div>*/ ?>

                        <div class="col-md-12">
                            <div class="panel panel-warning">
                                <div class="panel-heading"><?= lang('please_select_these_before_adding_product') ?></div>
                                <div class="panel-body" style="padding: 5px;">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <?= lang("supplier", "posupplier"); ?>
                                            <div class="input-group">
                                                <input type="hidden" name="supplier" value="" id="posupplier"
                                                       class="form-control" style="width:100%;"
                                                       placeholder="<?= lang("select") . ' ' . lang("supplier") ?>">

                                                <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;">
                                                    <a href="#" id="removeReadonly">
                                                        <i class="fa fa-unlock" id="unLock"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <input type="hidden" name="supplier_id" value="" id="supplier_id" class="form-control">
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="col-md-12" id="sticker">
						<div style="margin-top:30px;margin-bottom: 5px;">
								<h2><i class="fa fa-cubes"></i><?= lang("order_items"); ?> *</h2>
							</div>
							<p class="code"><code><?php echo lang('suggestion'); ?></code> <?php echo lang("Add item no need to specify product name or product code, press SPACE BAR to display all products list."); ?></p>
                            <div class="well well-sm">
                                <div class="form-group" style="margin-bottom:0;">
                                    <div class="input-group wide-tip">
                                        <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;">
                                            <i class="fa fa-2x fa-barcode addIcon"></i></a></div>
                                        <?php echo form_input('add_item', '', 'class="form-control input-lg" id="add_item" placeholder="' . $this->lang->line("add_product_to_order") . '"'); ?>
                                        <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;">
                                            <a href="#" id="addManually"><i class="fa fa-2x fa-plus-circle addIcon"
                                                                            id="addIcon"></i></a></div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="control-group table-group">
                                <label class="table-label"><?= lang("order_items"); ?></label>

                                <div class="controls table-controls">
                                    <table id="poTable"
                                           class="table items table-striped table-bordered table-condensed table-hover sortable_table">
                                        <thead>
                                        <tr>
                                            <th class="col-md-4"><?= lang('product') . ' (' . lang('code') .' - '.lang('name') . ')'; ?></th>
                                            <?php
                                            if ($Settings->product_expiry) {
                                                echo '<th class="col-md-1">' . $this->lang->line("expiry_date") . '</th>';
                                            }
                                            ?>
                                            <th class="col-md-1"><?= lang("net_unit_cost"); ?></th>
                                            <th class="col-md-1"><?= lang("quantity"); ?></th>
                                            <th class="col-md-1 rec_con"><?= lang("received"); ?></th>
                                            <?php
                                            if ($Settings->product_discount) {
                                                echo '<th class="col-md-1">' . $this->lang->line("discount") . '</th>';
                                            }
                                            ?>
                                            <?php
                                            if ($Settings->tax1) {
                                                echo '<th class="col-md-1">' . $this->lang->line("product_tax") . '</th>';
                                            }
                                            ?>
                                            <th><?= lang("subtotal"); ?> (<span
                                                    class="currency"><?= $default_currency->code ?></span>)
                                            </th>
                                            <th style="width: 30px !important; text-align: center;">
                                                <i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody></tbody>
                                        <tfoot></tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group">
						<div class="col-md-12">
							<div class="button_promotion" style="display:none;">
								<button type="button" class="btn btn-info" data-toggle="popover" data-html="true"  id="promotion"  data-content="<p><?php echo lang('Are you sure?');?></p><button type='button' class='btn btn-danger' id='yes_pro' data-action='delete' value='yes'><?php echo lang('Yes I\'m sure');?></button> <button type='button' class='btn btn-default' id='no_pro' value='no'><?php echo lang('No');?></button>"><?php echo lang("Use Promotion");?></button>
								<button type="button" class="btn btn-danger" id="cancel_promotion"><?php echo lang("Cancel");?></button>
							</div>
						</div>
					</div>
					<div class="clearfix"></div>

					<div class="col-md-4 discount">
						<div class="form-group">
							<h2 style="margin-bottom:3px;"><i class="fa fa-thumb-tack"></i><?= lang("order_discount"); ?></h2>
							<?php echo form_input('discount', $inv->order_discount, 'class="form-control input-tip" id="podiscount"'); ?>
						</div>
					</div>

                        <input type="hidden" name="total_items" value="" id="total_items" required="required"/>
					<div class="clearfix"></div>
					
					<div class="shipment">
						<div style="margin-top:10px;margin-bottom: 5px;">
							<h2><i class="fa fa-truck"></i><?= lang("shipment"); ?></h2>
						</div>
						<div class="well clear">
						<div class="col-md-2">
							<div class="form-group">
								<?= lang("shipping_title", "shipping_title"); ?>
								<?php 
									echo form_dropdown('shippingtitle', $shippingtitle, $inv->delivery_type, 'class="form-control input-tip" required="required" id="shippingtitle"'); ?>
							</div>
						</div>

						
						<?php if($Settings->default_shipping == "weight"){ ?>
						<div class="col-md-1 weight-set">
							<div class="form-group">
								<?= lang("weight_total", "slweight"); ?>
								<?php echo form_input('weight', '', 'class="form-control input-tip" id="weight"'); ?>
							</div>
						</div>
						<?php }  ?>

						<div style="display:none" class="updateship"><a href="javascript:void(0)" id="updateship">updateship</a></div>
						
						<div class="col-md-2">
							<div class="form-group">
								<?= lang("shipping", "poshipping"); ?>
								<?php echo form_input('shipping_price', $inv->shipping, 'class="form-control input-tip" id="poshipping"'); ?>
								<?php #echo form_input('shipping', '', 'class="form-control input-tip" id="slshipping"'); ?>

							</div>
						</div>
						
						
						</div>
						<div id="bottom-total" class="well well-sm" style="margin-bottom: 0;">
							<table class="table table-bordered table-condensed totals" style="margin-bottom:0;">
								<tr class="warning">
									<td><?= lang('items') ?> <span class="totals_val pull-right" id="titems">0</span></td>
									<td><?= lang('shipping') ?> <span class="totals_val pull-right" id="tship">0.00</span></td>
									<td><?= lang('grand_total') ?> <span class="totals_val pull-right" id="gtotal">0.00</span></td>
								</tr>
							</table>
						</div>
					</div>
					
					<div class="clearfix"></div>
							<div style="margin-top:30px;margin-bottom: 5px;" class="payment_title">
								<h2><i class="fa fa-credit-card"></i><?= lang("payment_confirm"); ?></h2>
							</div>

							<div class="">
							<div class="well cleartl payment_form" style="display: inline-block; width: 100%;">
								<div class="col-md-6">
										<div class="form-group">
											<?= lang("bank_to", "slbank_to"); ?>
											<?php
											//print_r($bank);
											$banks[""] = "";
											
													foreach ($bank as $bnk) {
														//print_r($bnk);
														$banks[$bnk->id] = "ธนาคาร : ".$bnk->bank." ชื่อบัญชี : ".$bnk->account_name." เลขที่บัญชี : ".$bnk->account_number;
													}
											echo form_dropdown('bank_to', $banks, $inv->bank_to, 'class="form-control input-tip" required="required" id="slbank_to"'); ?>
										</div>
								</div>

								<div class="col-md-6">
									<div class="row">
										<div class="col-md-8">
										<div class="form-group">
											<?= lang("transfer_date", "sldate"); ?>
											<?php $date = date('d-m-Y'); ?>
											<?php echo form_input('date_cf_payment', date("d-m-Y", strtotime($inv->date_cf_payment)), 'class="form-control input-tip date"  id="date_cf_payment" required="required"   data-bv-notempty-message="'.lang("Enter the correct date").'"'); ?>
										</div>
										</div>
										<div class="col-md-4">
										<div class="form-group">
											<?php $time = date('H:i'); ?>
											<?= lang("time", "sltime"); ?>
											<?php echo form_input('time_cf_payment',date("H:i", strtotime($inv->date_cf_payment)), 'class="form-control input-tip time2" data-timepicker="" autocomplete="off" placeholder="__:__"  id="time_cf_payment" required="required" data-bv-notempty-message="'.lang("Enter the correct time").'"'); ?>
										</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<?= lang("transfer_total", "sltotal_cf_payment"); ?>
										<?php echo form_input('total_cf_payment', $inv->total_cf_payment, 'class="form-control input-tip" required="required" id="sltotal_cf_payment"'); ?>

									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
										<?= lang("document", "document") ?>
										<input id="document"  type="file" data-browse-label="<?= lang('browse'); ?>" name="document" data-show-upload="false"
											   data-show-preview="false" class="form-control file">
									</div>
								</div>
							</div>
							</div>
		
					<?php /*
                        <div class="col-md-12">
					
                            <div class="form-group">
                                <input type="checkbox" class="checkbox" id="extras" value=""/>
                                <label for="extras" class="padding05"><?= lang('more_options') ?></label>
                            </div>
                            <div class="row" id="extras-con" style="display: none;">
                           <?php /*     <?php if ($Settings->tax1) { ?>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <?= lang('order_tax', 'potax2') ?>
                                            <?php
                                            $tr[""] = "";
                                            foreach ($tax_rates as $tax) {
                                                $tr[$tax->id] = $tax->name;
                                            }
                                            echo form_dropdown('order_tax', $tr, "", 'id="potax2" class="form-control input-tip select" style="width:100%;"');
                                            ?>
                                        </div>
                                    </div>
                                <?php } ?> 

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?= lang("discount_label", "podiscount"); ?>
                                        <?php echo form_input('discount', '', 'class="form-control input-tip" id="podiscount"'); ?>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?= lang("shipping", "poshipping"); ?>
                                        <?php echo form_input('shipping', '', 'class="form-control input-tip" id="poshipping"'); ?>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?= lang("payment_term", "popayment_term"); ?>
                                        <?php echo form_input('payment_term', '', 'class="form-control tip" data-trigger="focus" data-placement="top" title="' . lang('payment_term_tip') . '" id="popayment_term"'); ?>
                                    </div>
                                </div>
                            </div>
						
                            <div class="clearfix"></div>
                            <div class="form-group">
                                <?= lang("note", "ponote"); ?>
                                <?php echo form_textarea('note', (isset($_POST['note']) ? $_POST['note'] : ""), 'class="form-control" id="ponote" style="margin-top: 10px; height: 100px;"'); ?>
                            </div>

                        </div>
						*/ ?>
                        <div class="col-md-12">
                            <div
                                class="from-group"><?php echo form_submit('edit_pruchase', $this->lang->line("submit"), 'id="edit_pruchase" class="btn btn-success" style="padding: 6px 15px; margin:15px 0;"'); ?>
                                <button type="button" class="btn btn-danger" id="reset"><?= lang('reset') ?></button>
                            </div>
                        </div>
                    </div>
                </div>

                <?php echo form_close(); ?>

            </div>

        </div>
    </div>
</div>

<div class="modal" id="prModal" tabindex="-1" role="dialog" aria-labelledby="prModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i
                            class="fa fa-2x">&times;</i></span><span class="sr-only"><?=lang('close');?></span></button>
                <h4 class="modal-title" id="prModalLabel"></h4>
            </div>
            <div class="modal-body" id="pr_popover_content">
                <form class="form-horizontal" role="form">
                    <?php if ($Settings->tax1) { ?>
                        <div class="form-group">
                            <label class="col-sm-4 control-label"><?= lang('product_tax') ?></label>
                            <div class="col-sm-8">
                                <?php
                                $tr[""] = "";
                                foreach ($tax_rates as $tax) {
                                    $tr[$tax->id] = $tax->name;
                                }
                                echo form_dropdown('ptax', $tr, "", 'id="ptax" class="form-control pos-input-tip" style="width:100%;"');
                                ?>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="form-group">
                        <label for="pquantity" class="col-sm-4 control-label"><?= lang('quantity') ?></label>

                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="pquantity">
                        </div>
                    </div>
                    <?php if ($Settings->product_expiry) { ?>
                        <div class="form-group">
                            <label for="pexpiry" class="col-sm-4 control-label"><?= lang('product_expiry') ?></label>

                            <div class="col-sm-8">
                                <input type="text" class="form-control date" id="pexpiry">
                            </div>
                        </div>
                    <?php } ?>
                    <div class="form-group">
                        <label for="punit" class="col-sm-4 control-label"><?= lang('product_unit') ?></label>
                        <div class="col-sm-8">
                            <div id="punits-div"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="poption" class="col-sm-4 control-label"><?= lang('product_option') ?></label>
                        <div class="col-sm-8">
                            <div id="poptions-div"></div>
                        </div>
                    </div>
                    <?php if ($Settings->product_discount) { ?>
                        <div class="form-group">
                            <label for="pdiscount"
                                   class="col-sm-4 control-label"><?= lang('product_discount') ?></label>

                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="pdiscount">
                            </div>
                        </div>
                    <?php } ?>
                    <div class="form-group">
                        <label for="pcost" class="col-sm-4 control-label"><?= lang('unit_cost') ?></label>

                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="pcost">
                        </div>
                    </div>
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th style="width:25%;"><?= lang('net_unit_cost'); ?></th>
                            <th style="width:25%;"><span id="net_cost"></span></th>
                            <th style="width:25%;"><?= lang('product_tax'); ?></th>
                            <th style="width:25%;"><span id="pro_tax"></span></th>
                        </tr>
                    </table>
                    <div class="panel panel-default">
                        <div class="panel-heading"><?= lang('calculate_unit_cost'); ?></div>
                        <div class="panel-body">

                            <div class="form-group">
                                <label for="pcost" class="col-sm-4 control-label"><?= lang('subtotal') ?></label>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="psubtotal">
                                        <div class="input-group-addon" style="padding: 2px 8px;">
                                            <a href="#" id="calculate_unit_price" class="tip" title="<?= lang('calculate_unit_cost'); ?>">
                                                <i class="fa fa-calculator"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" id="punit_cost" value=""/>
                    <input type="hidden" id="old_tax" value=""/>
                    <input type="hidden" id="old_qty" value=""/>
                    <input type="hidden" id="old_cost" value=""/>
                    <input type="hidden" id="row_id" value=""/>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="editItem"><?= lang('submit') ?></button>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="mModal" tabindex="-1" role="dialog" aria-labelledby="mModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i
                            class="fa fa-2x">&times;</i></span><span class="sr-only"><?=lang('close');?></span></button>
                <h4 class="modal-title" id="mModalLabel"><?= lang('add_standard_product') ?></h4>
            </div>
            <div class="modal-body" id="pr_popover_content">
                <div class="alert alert-danger" id="mError-con" style="display: none;">
                    <!--<button data-dismiss="alert" class="close" type="button">×</button>-->
                    <span id="mError"></span>
                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <div class="form-group">
                            <?= lang('product_code', 'mcode') ?> *
                            <input type="text" class="form-control" id="mcode">
                        </div>
                        <div class="form-group">
                            <?= lang('product_name', 'mname') ?> *
                            <input type="text" class="form-control" id="mname">
                        </div>
                        <div class="form-group">
                            <?= lang('category', 'mcategory') ?> *
                            <?php
                            $cat[''] = "";
                            foreach ($categories as $category) {
                                $cat[$category->id] = $category->name;
                            }
                            echo form_dropdown('category', $cat, '', 'class="form-control select" id="mcategory" placeholder="' . lang("select") . " " . lang("category") . '" style="width:100%"')
                            ?>
                        </div>
                        <div class="form-group">
                            <?= lang('unit', 'munit') ?> *
                            <input type="text" class="form-control" id="munit">
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="form-group">
                            <?= lang('cost', 'mcost') ?> *
                            <input type="text" class="form-control" id="mcost">
                        </div>
                        <div class="form-group">
                            <?= lang('price', 'mprice') ?> *
                            <input type="text" class="form-control" id="mprice">
                        </div>

                        <?php if ($Settings->tax1) { ?>
                            <div class="form-group">
                                <?= lang('product_tax', 'mtax') ?>
                                <?php
                                $tr[""] = "";
                                foreach ($tax_rates as $tax) {
                                    $tr[$tax->id] = $tax->name;
                                }
                                echo form_dropdown('mtax', $tr, "", 'id="mtax" class="form-control input-tip select" style="width:100%;"');
                                ?>
                            </div>
                            <div class="form-group all">
                                <?= lang("tax_method", "mtax_method") ?>
                                <?php
                                $tm = array('0' => lang('inclusive'), '1' => lang('exclusive'));
                                echo form_dropdown('tax_method', $tm, '', 'class="form-control select" id="mtax_method" placeholder="' . lang("select") . ' ' . lang("tax_method") . '" style="width:100%"')
                                ?>
                            </div>
                        <?php } ?>
                    </div>
                </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="addItemManually"><?= lang('submit') ?></button>
            </div>
        </div>
    </div>
</div>
<script>
		function shippingtitle(delivery_type){

			if(delivery_type == null){
				delivery_type = jQuery('#shippingtitle > option:first-child').val();
			}
			//console.log(delivery_type);

			var item_val = count-1;
			var condition = '<?php echo $Settings->default_shipping; ?>';
			var is_total = 0;

			//console.log(condition);
			if(condition == 'price'){
				is_total = total;
			}else if(condition == 'weight'){
				is_total = jQuery("#weight").val();
			}else{
				is_total =  item_val;
			}
			//console.log(is_total);

			try {

				console.log(is_total);
                $.ajax({
                    type: 'get',
                    url: '<?= admin_url('sales/shipping'); ?>',
                    dataType: "json",
                    data: {
                        condition_name: condition,
						is_price: is_total,
						delivery_type: delivery_type,
                    },
                    success: function (data) {
						console.log(data[0].price);
						if(data[0].price == 'null'){

						}else{
							var shipping = formatDecimal(data[0].price);
							jQuery('#poshipping').val(shipping);
							//console.log(podiscount);
							var gtotal = (total + shipping)  -  podiscount;
							var gtotal = (((total + invoice_tax)) + shipping) - podiscount;

							$('#sltotal_cf_payment').val(formatMoney(gtotal));
								$('#gtotal').text(formatMoney(gtotal));
								$('#tship').text(formatMoney(shipping));

						}
                    }, error: function () {

					}
                });
			} catch (e) {
				console.log(e.message());
			}
		}

		function clear_promotion(){
			jQuery(".button_promotion").hide();
			jQuery("#hidden_sale_price").remove();
			//jQuery("#podiscount").attr("value",0);
			jQuery("#promotion").removeAttr("disabled");
			jQuery(".discount_text").hide();
		}
		function clear_promotion_btn(){
			jQuery("#promotion").removeAttr("disabled");
			//jQuery("#podiscount").attr("value",0);
			var podiscount = jQuery("#podiscount").val();
			jQuery('#gtotal').text(formatMoney(total + shipping));
			jQuery(".discount_text").hide();
			jQuery('#sltotal_cf_payment').val(formatMoney(total + shipping));
		}

		function combo_check(){

			var array_sku = new Array();
			var sku = "";
			jQuery('#poTable tbody input.rcode').each(function(){
				if(array_sku.includes(jQuery(this).val())==false){
					array_sku.push(jQuery(this).val());
				}
			})

			$.ajax({
			  method: "GET",
			  url: "<?= admin_url('purchases/combo_check'); ?>",
			  data: { sku: array_sku }
			})
			  .done(function( data ) {
				console.log(data);

			//	clear_promotion();
				if(!!data){
			<?php if(!$this->Settings->overselling):?> if(jQuery('#poTable tbody tr').hasClass("danger") == false){ <?php endif; ?>
						var combo_product = [];
						for(var i=0;i<data.length;i++){
							if(data[i].combo_promotion != null){
								combo_product = data[i];
							}
						}
						if(combo_product.sum_price){
							var promotion_product_price = 0;
							var value_not_same = false;
							jQuery("#poTable").each(function(){
									jQuery(".button_promotion").show();
									if(combo_product.combo_promotion.combo_mix != "1"){
										jQuery(this).parent().append('<input type="hidden"  id="hidden_sale_price" value="'+combo_product.sum_price+'"></input>');
									}else{
										var count_sku_found = 0;
										var product_item_qty = 0;
										var min_qty = 0;
										var sum_qty = 0;
										var sum_item_price = 0;
										var sum_price = 0;
										for(var t=0;t<combo_product.combo_mix_sku.length;t++){
											jQuery("#poTable tbody tr .rcode").each(function(){
												if(combo_product.combo_mix_sku[t].item_code==jQuery(this).val()){
													promotion_product_price = parseInt(promotion_product_price) + parseInt(combo_product.combo_mix_sku[t].base_price);
													product_item_qty = jQuery(this).parents("tr").find(".rbqty").val();
													product_item_price = jQuery(this).parents("tr").find(".realucost").val() * product_item_qty;
													sum_qty = parseInt(sum_qty) + parseInt(product_item_qty);
													sum_item_price = parseInt(sum_item_price) + parseInt(product_item_price);
													if(count_sku_found=="0"){
														min_qty = product_item_qty;
													}else{
														if(min_qty!=product_item_qty){
														//	value_not_same = true;
														}
													}
													count_sku_found = parseInt(count_sku_found) + 1;
												}
											});
										}

										if(value_not_same==false){
											for(var cw = 0;cw<combo_product.warehouse_promotion.length;cw++){
												if(parseInt(sum_qty) == parseInt(combo_product.warehouse_promotion[cw].quantity)){
													sum_price = combo_product.warehouse_promotion[cw].price;
												}
											}
											console.log("promotion_product_price = "+promotion_product_price);
											console.log("sum_price = "+sum_price);
											if(sum_price > 0){
												jQuery(this).parent().append('<input type="hidden"  id="hidden_sale_price" value="'+(sum_item_price - sum_price)+'"></input>');
												jQuery(".unequal").remove();
											}else{
												clear_promotion();
											}
										}else{
											jQuery(this).parent().append('<input type="hidden"  id="hidden_sale_price" value="0"></input>');
											jQuery(".button_promotion .btn-info").attr("disabled","disabled");
											jQuery(".unequal").remove();
											jQuery(".button_promotion").prepend("<div class='unequal alert alert-danger' role='alert'><?php echo lang('Products to use promotion are not equal.');?></div>");
										}
									}
									jQuery("#poTable tbody tr .rcode").each(function(){
										var item_code = jQuery(this).val();
										for(var i = 0;i<combo_product.combo_promotion.length;i++){
											if(combo_product.combo_promotion[i].item_code==item_code && combo_product.combo_promotion[i].sale_price > 0){
												jQuery(this).parent().children(".sname").html(jQuery(this).parent().children(".sname").text() + "<span class='discount_text' style='display:none;'> - <b>(<?php echo lang('Discount');?> : " + formatDecimal(combo_product.combo_promotion[i].sale_price,2) +" )</b></span>");
											}
										}
									});
							});
						}
					<?php if(!$this->Settings->overselling):?> } <?php endif; ?>
				}
			});

		}

		$('#shippingtitle').on('change', function(){
			var delivery_type = this.value;
			shippingtitle(delivery_type);

		});

		$("#updateship").click(function(){
			var delivery_type = $('#shippingtitle').value;
			shippingtitle(delivery_type);
			combo_check();
		});

		setTimeout(function(){
			shippingtitle();
			// $(".ui-autocomplete, #poTable i.fa-times").on('click', function(){
				// shippingtitle();
			// });
		},800);

		jQuery(document).on('click','#no_pro',function(){
			jQuery('#promotion + .popover').remove();
			jQuery("#promotion").removeAttr("disabled");
		});

		jQuery(document).on('click','#yes_pro',function(){
			if(jQuery(this).val()=="yes"){
				jQuery("#podiscount").attr("value",jQuery("#hidden_sale_price").val());
				var gtotal = total;
				var podiscount = jQuery("#podiscount").val();
				jQuery('#gtotal').text(formatMoney((gtotal + shipping) - podiscount));
				jQuery('#sltotal_cf_payment').val(formatMoney((gtotal + shipping) - podiscount));
				jQuery("#promotion").attr("disabled","disabled");
				jQuery('#promotion + .popover').remove();
				jQuery(".discount_text").show();
			}
		});

		jQuery(document).on('click','#cancel_promotion',function(){
			clear_promotion_btn();
		});

</script>